package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.models.dao.IOrganizacionDao;
import com.example.demo.models.entity.Organizacion;

@Service
public class OrganizacionServiceImpl implements IOrganizacionService{

	@Autowired
	private IOrganizacionDao organizacionDao;
	
	@Override
	@Transactional(readOnly=true)
	public List<Organizacion> findAll() {
		return (List<Organizacion>) organizacionDao.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Organizacion findbyId(Long id) {
		return organizacionDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Organizacion save(Organizacion organizacion) {
		Long count = organizacionDao.count() +1 ;
		String numero = count.toString();
		StringBuilder sb = new StringBuilder(3);
		if (numero.length() == 1) {
			sb.append("00").append(numero);
		} else  if (numero.length() == 2) {
			sb.append("0").append(numero);
		}
		String idExterno = new String(
			organizacion.getNombre().substring(0, 3).toUpperCase()
			.concat(organizacion.getTelefono().substring(organizacion.getTelefono().length()-4, organizacion.getTelefono().length()))
			.concat("NE")
			.concat(sb.toString())
		);
		organizacion.setIdExterno(idExterno);
		return organizacionDao.save(organizacion);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		organizacionDao.deleteById(id);
	}
	
}
