package com.example.demo.service;

import java.util.List;

import com.example.demo.models.entity.Organizacion;


public interface IOrganizacionService {
	
	public List<Organizacion> findAll();
	
	public Organizacion findbyId(Long id);
	
	public Organizacion save(Organizacion orgnizacion);
	
	public void delete(Long id);
}
