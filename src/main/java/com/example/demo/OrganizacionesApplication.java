package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrganizacionesApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrganizacionesApplication.class, args);
	}

}
