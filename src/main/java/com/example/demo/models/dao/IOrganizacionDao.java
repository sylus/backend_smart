package com.example.demo.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.models.entity.Organizacion;

public interface IOrganizacionDao extends CrudRepository<Organizacion, Long> {
	
	@Query("select nombre from Organizacion")
	public List<String> findAllNames();

}
