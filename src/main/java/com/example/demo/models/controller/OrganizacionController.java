package com.example.demo.models.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.entity.Organizacion;
import com.example.demo.service.IOrganizacionService;

@CrossOrigin(origins={"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class OrganizacionController {

	@Autowired
	private IOrganizacionService organizacionService;
	
	@GetMapping("/organizaciones")
	public List<Organizacion> index(){
		return organizacionService.findAll();
	}
	
	@GetMapping("/organizaciones/{id}")
	public ResponseEntity<?> show (@PathVariable Long id) {
		Organizacion organizacion = null;
		Map<String, Object> response = new HashMap<>();
		try {
			organizacion = organizacionService.findbyId(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (organizacion == null) {
			response.put("mensaje", "La organización ID: ".concat(id.toString().concat(" no existe en la base de datos.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Organizacion>(organizacion, HttpStatus.OK);
	}
	
	@PostMapping("/organizaciones")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> create(@RequestBody Organizacion organizacion) {
		Organizacion organizacionNuevo = null;
		Map<String, Object> response = new HashMap<>();
		try {
			organizacionNuevo = organizacionService.save(organizacion);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La organizacion ha sido creada con éxito");
		response.put("organizacion", organizacionNuevo);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PutMapping("/organizaciones/{id}")
	public ResponseEntity<?> update(@RequestBody Organizacion organizacion, @PathVariable Long id) {
		Organizacion organizacionActual = organizacionService.findbyId(id);
		Organizacion organizacionActualizada = null;
		Map<String, Object> response = new HashMap<>();
		if (organizacionActual == null) {
			response.put("mensaje", "Error: No se pudo editar, la organización ID: ".concat(id.toString().concat(" no existe en la base de datos.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		try {
			organizacionActual.setNombre(organizacion.getNombre());
			organizacionActual.setDireccion(organizacion.getDireccion());
			organizacionActual.setTelefono(organizacion.getTelefono());
			organizacionActual.setCodigoEncriptacion(organizacion.getCodigoEncriptacion());
			organizacionActualizada = organizacionService.save(organizacionActual);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar la organización en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La organizacion ha sido actualizada con éxito");
		response.put("organizacion", organizacionActualizada);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/organizaciones/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		try {
			organizacionService.delete(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar la organización de la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "La organización ha sido eliminada con exito");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
}
